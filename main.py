from fastapi import FastAPI

app = FastAPI()


@app.get
@app.get("/items/{item_id}")
async def get_items(item_id: int):
    return {"item_id": item_id}
